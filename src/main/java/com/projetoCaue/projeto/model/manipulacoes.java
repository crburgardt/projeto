/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetoCaue.projeto.model;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import java.util.Arrays;

/**
 *
 * @author Cauê
 */
public class manipulacoes {
    

    public static String[] manipulacoes(String stri){
        String trans = stri; 
        String separada[];
        trans = stri.replaceAll("\\D", "");
        separada = stri.split("\\D");
       
         return separada;
    }
    
     public static int[] convert(String stri){
        
        String trans[]; 
        trans = manipulacoes(stri);
        int vet[] = new int[trans.length -1 ];
        vet[1] = Integer.parseInt(String.valueOf(trans[1]));
        for(int i = 1; i < trans.length; i++){
             vet[i-1] = Integer.parseInt(String.valueOf(trans[i]));
        }
       
         return vet;
    }
    
    public static String converStr(int[] vet){
         String fina ="";
         int contr = vet.length;
         StringBuilder str = new StringBuilder();
        
        fina = Arrays.toString(vet);
         return fina;
    } 
    public static String invert(String vet){
        
       int vetnum[] = null;
       int contr; 
       String fin;

       vetnum = convert(vet);
     
       contr = vetnum.length;
       int regr = contr-1;
       int vet2[] = new int[contr];
       for(int i =0; i< contr; i++){
           vet2[regr] = vetnum[i];
           regr--;
       }
        fin = converStr(vet2);

         return fin;
    }
    
    public static String impares(String vet){
        
       int vetnum[] = null;
       int contr; 
       String fin;
       int b = 0;
       int j = 0;
       
       vetnum = convert(vet);
     
       contr = vetnum.length;
       int vet2[] = new int[contr];

        for(int i =0; i< contr; i++){
           if(vetnum[i]%2 !=  0){
               b++;
               vet2[i] = vetnum[i];
           }
       }
        
       int vet3[] = new int[b];

        for(int i =0; i< contr; i++){
           if(vetnum[i]%2 !=  0){
               if(vet2[i] != 0){
               vet3[j] = vet2[i];
               j++;
               }
           }
       }
        j = 0;
         fin = converStr(vet3);
         return fin;
    }
    
     public static String pares(String vet){
        
       int vetnum[] = null;
       int contr; 
       String fin;
       int b = 0;
       int j = 0;
       vetnum = convert(vet);
     
       contr = vetnum.length;

       int vet2[] = new int[contr];

        for(int i =0; i< contr; i++){
           if(vetnum[i]%2 ==  0){
               b++;
               vet2[i] = vetnum[i];
           }
       }
        
       int vet3[] = new int[b];

        for(int i =0; i< contr; i++){
           if(vetnum[i]%2 ==  0){
               if(vet2[i] != 0){
               vet3[j] = vet2[i];
               j++;
               }
           }
       }
        j = 0;
         fin = converStr(vet3);
         return fin;
    }
    
}
