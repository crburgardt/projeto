/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetoCaue.projeto.resource;
import com.projetoCaue.projeto.model.manString;
import com.projetoCaue.projeto.model.manipulacoes;
import java.util.HashMap;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cauê
 */
import com.projetoCaue.projeto.model.model;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping(value = "/")
public class resource {
     
    
    private Map<String, model> modelos;
    
    String lisRev ="";
    
    public resource(){           
        modelos = new HashMap<>();         
        model ver = new model(lisRev); 
        modelos.put("1", ver);   
    }
    
    @RequestMapping(value = "/listaReversa", method=RequestMethod.GET)
    ResponseEntity<List<model>> listaReversa(@RequestParam(value="lista", required=false) String lista) {
             
             lisRev = manipulacoes.invert(lista);
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
     
 @RequestMapping(value ="/imprimirImpares" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> numerosimpares(@RequestParam(value="lista", required=false) String lista) {
             
             lisRev = manipulacoes.impares(lista);           
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
    @RequestMapping(value ="/imprimirPares" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> numerosPares(@RequestParam(value="lista", required=false) String lista) {
             
             lisRev = manipulacoes.pares(lista);            
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
     @RequestMapping(value ="/tamanho" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> Tamanho(@RequestParam(value="palavra", required=false) String lista) {

             lisRev = manString.contar(lista); 
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
     @RequestMapping(value ="/maisculas" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> LMaiusculas(@RequestParam(value="palavra", required=false) String lista) {
            
             System.out.println("maiusculas: " + lista);
             
             lisRev = manString.paraMaius(lista);
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
     @RequestMapping(value ="/vogais" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> lVogais(@RequestParam(value="palavra", required=false) String lista) {
            
             System.out.println("vogais: " + lista);
             lisRev = manString.paraVogais(lista);
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
     @RequestMapping(value ="/consoantes" ,method=RequestMethod.GET)
    ResponseEntity<List<model>> lConsoantes(@RequestParam(value="palavra", required=false) String lista) {
            
             lisRev = manString.paraConsoantes(lista);
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
     @RequestMapping(value ="/nomeBibliografico")
    ResponseEntity<List<model>> nomeBibliografico(@PathVariable(value="nome", required=false) String lista) {
            
             System.out.println("nome bibl: " + lista);
             
             lisRev= lista;
             
             model ver = new model(lisRev);
             modelos.put("1", ver); 
      
        return listar();
    }
    
    @RequestMapping(value = "/",method=RequestMethod.GET)
    public ResponseEntity<List<model>> listar() {
    return new ResponseEntity<List<model>>(new ArrayList<model>(modelos.values()), HttpStatus.OK);
  }
}
